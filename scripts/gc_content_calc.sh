gffread lncrna_annotation/lncrna.gtf -g reference_genome/ensembl_chok1_genome.fa  -w manuscript_plotting/figure2/chok1.lncrna.fasta​
gffread stringtie/stringtie.filtered.protein.coding.gtf -g reference_genome/ensembl_chok1_genome.fa  -w manuscript_plotting/figure2/chok1.mrna.fasta​


bioawk -c 'fastx' '{print gc($seq)*100"\t""lncRNA"}' manuscript_plotting/figure2/chok1.lncrna.fasta >> manuscript_plotting/figure2/gc.content.txt
bioawk -c 'fastx' '{print gc($seq)*100"\t""mRNA"}' manuscript_plotting/figure2/chok1.mrna.fasta >> manuscript_plotting/figure2/gc.content.txt
