
mkdir coverage_plots
mkdir tmp
samtools faidx reference_genome/ensembl_chok1_genome.fa
cut -f1,2 reference_genome/ensembl_chok1_genome.fa.fai > reference_genome/sizes.genome

cat ../data/sample_names.txt | while read sample; do
bamCoverage -b mapped/"$sample"Aligned.sortedByCoord.out.bam -o coverage_plots/"$sample"_forward.region.bw \
--outFileFormat bigwig \
--filterRNAstrand forward \
--normalizeUsing BPM \
--binSize 1 \
  --region JH000378.1:453578:576,999 \
--numberOfProcessors 28
done

cat ../data/sample_names.txt | while read sample; do
bamCoverage -b mapped/"$sample"Aligned.sortedByCoord.out.bam -o coverage_plots/"$sample"_reverse.region.bw \
--outFileFormat bigwig \
--filterRNAstrand reverse \
--normalizeUsing BPM \
--binSize 1 \
--region JH000378.1:453578:576,999 \
--numberOfProcessors 28
done

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/31_forward_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/REP31_1_forward.region.bw \
coverage_plots/REP31_2_forward.region.bw \
coverage_plots/REP31_3_forward.region.bw \
coverage_plots/REP31_4_forward.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/37_forward_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/REP37_1_forward.region.bw \
coverage_plots/REP37_2_forward.region.bw \
coverage_plots/REP37_3_forward.region.bw \
coverage_plots/REP37_4_forward.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/31_reverse_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/REP31_1_reverse.region.bw \
coverage_plots/REP31_2_reverse.region.bw \
coverage_plots/REP31_3_reverse.region.bw \
coverage_plots/REP31_4_reverse.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/37_reverse_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/REP37_1_reverse.region.bw \
coverage_plots/REP37_2_reverse.region.bw \
coverage_plots/REP37_3_reverse.region.bw \
coverage_plots/REP37_4_reverse.region.bw


################################################################################

cat ../data/sample_names.txt | while read sample; do
bamCoverage -b mapped/"$sample"Aligned.sortedByCoord.out.bam -o coverage_plots/timeless/"$sample"_forward.region.bw \
--outFileFormat bigwig \
--filterRNAstrand forward \
--normalizeUsing BPM \
--binSize 1 \
  --region JH000638.1:312470:335676 \
--numberOfProcessors 28
done

cat ../data/sample_names.txt | while read sample; do
bamCoverage -b mapped/"$sample"Aligned.sortedByCoord.out.bam -o coverage_plots/timeless/"$sample"_reverse.region.bw \
--outFileFormat bigwig \
--filterRNAstrand reverse \
--normalizeUsing BPM \
--binSize 1 \
--region JH000638.1:312470:335676 \
--numberOfProcessors 28
done

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/timeless/31_forward_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/timeless/REP31_1_forward.region.bw \
coverage_plots/timeless/REP31_2_forward.region.bw \
coverage_plots/timeless/REP31_3_forward.region.bw \
coverage_plots/timeless/REP31_4_forward.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/timeless/37_forward_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/timeless/REP37_1_forward.region.bw \
coverage_plots/timeless/REP37_2_forward.region.bw \
coverage_plots/timeless/REP37_3_forward.region.bw \
coverage_plots/timeless/REP37_4_forward.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/timeless/31_reverse_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/timeless/REP31_1_reverse.region.bw \
coverage_plots/timeless/REP31_2_reverse.region.bw \
coverage_plots/timeless/REP31_3_reverse.region.bw \
coverage_plots/timeless/REP31_4_reverse.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/timeless/37_reverse_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/timeless/REP37_1_reverse.region.bw \
coverage_plots/timeless/REP37_2_reverse.region.bw \
coverage_plots/timeless/REP37_3_reverse.region.bw \
coverage_plots/timeless/REP37_4_reverse.region.bw

################################################################################

cat ../data/sample_names.txt | while read sample; do
bamCoverage -b mapped/"$sample"Aligned.sortedByCoord.out.bam \
-o coverage_plots/cbx3/"$sample"_forward.region.bw \
--outFileFormat bigwig \
--filterRNAstrand forward \
--normalizeUsing BPM \
--binSize 1 \
  --region JH000116.1:654713:665132 \
--numberOfProcessors 28
done

cat ../data/sample_names.txt | while read sample; do
bamCoverage -b mapped/"$sample"Aligned.sortedByCoord.out.bam \
-o coverage_plots/cbx3/"$sample"_reverse.region.bw \
--outFileFormat bigwig \
--filterRNAstrand reverse \
--normalizeUsing BPM \
--binSize 1 \
--region JH000116.1:654713:665132 \
--numberOfProcessors 28
done

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/cbx3/31_forward_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/cbx3/REP31_1_forward.region.bw \
coverage_plots/cbx3/REP31_2_forward.region.bw \
coverage_plots/cbx3/REP31_3_forward.region.bw \
coverage_plots/cbx3/REP31_4_forward.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/cbx3/37_forward_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/cbx3/REP37_1_forward.region.bw \
coverage_plots/cbx3/REP37_2_forward.region.bw \
coverage_plots/cbx3/REP37_3_forward.region.bw \
coverage_plots/cbx3/REP37_4_forward.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/cbx3/31_reverse_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/cbx3/REP31_1_reverse.region.bw \
coverage_plots/cbx3/REP31_2_reverse.region.bw \
coverage_plots/cbx3/REP31_3_reverse.region.bw \
coverage_plots/cbx3/REP31_4_reverse.region.bw

#31 plus strand
./scripts/merge_bigwig.sh -g 100 -T tmp \
coverage_plots/cbx3/37_reverse_avg_region.bw \
reference_genome/sizes.genome \
coverage_plots/cbx3/REP37_1_reverse.region.bw \
coverage_plots/cbx3/REP37_2_reverse.region.bw \
coverage_plots/cbx3/REP37_3_reverse.region.bw \
coverage_plots/cbx3/REP37_4_reverse.region.bw
