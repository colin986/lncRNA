FEELnc_classifier.pl \
-i reference_genome/ensembl_chok1_genome.lncrnas.gtf \
-a reference_genome/ensembl_chok1_protein.gtf\
> lncrna_annotation/classification/ensembl_lncrna_v_protein_coding.txt


best_classifications<-read.table("lncrna_annotation/classification/ensembl_lncrna_v_protein_coding.txt",header=T, stringsAsFactors=F)
best_classifications<-best_classifications[best_classifications$isBest==1,]

modified_classification<-best_classifications[,c(1:5,8)]
position<-best_classifications$location
position[position=="exonic"] <- "overlapping"
position[position=="intronic"] <- "overlapping"
modified_classification$strand<-best_classifications$direction
modified_classification$position<-position

summary_class<-matrix(,dim(modified_classification)[1])
for (i in 1:dim(modified_classification)[1]){
  if ((modified_classification$strand[i] == "antisense") && (modified_classification$position[i]=="upstream") && (modified_classification$distance[i] <=2000)) {
summary_class[i]<-"divergent"
  } else if ((modified_classification$strand[i] == "antisense") && (modified_classification$position[i]=="upstream") && (modified_classification$distance[i] >2000)){
summary_class[i]<-"upstream antisense"
  } else if ((modified_classification$strand[i] == "antisense") && (modified_classification$position[i]=="downstream")){
summary_class[i]<-"downstream antisense"
  } else if ((modified_classification$strand[i] == "antisense") && (modified_classification$position[i]=="overlapping")){
summary_class[i]<-" antisense"
  } else if ((modified_classification$strand[i] == "sense") && (modified_classification$position[i]=="overlapping")){
summary_class[i]<-"overlap sense"
  } else if ((modified_classification$strand[i] == "sense") && (modified_classification$position[i]=="downstream")){
summary_class[i]<-"downstream sense"
  } else if ((modified_classification$strand[i] == "sense") && (modified_classification$position[i]=="upstream")){
summary_class[i]<-"upstream sense"
  }
}
