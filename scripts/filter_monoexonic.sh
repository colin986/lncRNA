#!/usr/bin/env bash
## anotation of lncRNAs via RFAM blast and syntheny with human and mouse

if (($# == 0)); then
        echo "Usage:"
        echo "-s lncRNA sequences"
        echo "-o = Output directory"
        exit 2
fi
while getopts s:g:o:l:k:a: option
  do
    case "${option}"
      in
      a) stringtie_gtf=${OPTARG};;
      k) ens_lncrna=${OPTARG};;
      l) lncrna_gtf=${OPTARG};;
      g) ref_gtf=${OPTARG};;
      s) lncRNA_fasta=${OPTARG};;
      o) OUTDIR=${OPTARG};;
    esac
done

mkdir $OUTDIR/classification
FEELnc_classifier.pl   -i $lncrna_gtf -a $ref_gtf > $OUTDIR/classification/first_classification.txt


#awk '{ if ($3 == "exon") print $0; }' lncrna_annotation/all_lncrna.gtf | grep -o "transcript_id [^;]\+;" | awk -v FS=" " '{ arr[substr($2, 2, length($2)-3)] += 1; } END { for (t in arr) { if (arr[t] > 1) { print t;} } }'

# all monexonic
mkdir $OUTDIR/monoexonic_filter

sed 's/\s/\t/g'  $lncrna_gtf | awk '$3 == "exon"'| datamash -s  -g 12 count 3 \
| awk '$2 < 2' |  tr -d \" | tr -d \; | awk '{print $1}' | sort > $OUTDIR/monoexonic_filter/monoexonic_transcripts.list

sed 's/\s/\t/g' $lncrna_gtf | awk '$3 == "exon"'| datamash -s  -g 12 count 3 \
| awk '$2 >= 2' |   tr -d \" | tr -d \; | awk '{print $1}' | sort > $OUTDIR/monoexonic_filter/not_monoexonic_transcripts.list

# determine monoexonic with hits in rfam, as well as synteny with human mouse
# rfam blast hits
awk '{print $1}' $OUTDIR/RFAM/lncrna.blastn.outfmt6 | sort > $OUTDIR/monoexonic_filter/rfam_hits_transcripts.list
comm -12 $OUTDIR/monoexonic_filter/rfam_hits_transcripts.list $OUTDIR/monoexonic_filter/monoexonic_transcripts.list >  $OUTDIR/monoexonic_filter/rfam_monoexonic_lncrnas.list
# rfam blast hits

awk '{print $1}' $OUTDIR/liftover/lncrna_cho_to_human.venn.list | sort > $OUTDIR/monoexonic_filter/human_synteny.list
comm -12 $OUTDIR/monoexonic_filter/human_synteny.list $OUTDIR/monoexonic_filter/monoexonic_transcripts.list >  $OUTDIR/monoexonic_filter/human_monoexonic_lncrnas.list

awk '{print $1}' $OUTDIR/liftover/lncrna_cho_to_mouse.venn.list | sort  > lncrna_annotation/monoexonic_filter/mouse_synteny.list
comm -12 $OUTDIR/monoexonic_filter/mouse_synteny.list $OUTDIR/monoexonic_filter/monoexonic_transcripts.list >  $OUTDIR/monoexonic_filter/mouse_monoexonic_lncrnas.list

# retain monoexonic discovered here but are classified as antisense to a protein
awk '$8 ==0 {print}' lncrna_annotation/classification/first_classification.txt | grep 'antisense' | awk '{print $3}' | uniq | sort > $OUTDIR/monoexonic_filter/feelnc_antisense_overlap.list
comm -12 $OUTDIR/monoexonic_filter/feelnc_antisense_overlap.list $OUTDIR/monoexonic_filter/monoexonic_transcripts.list > $OUTDIR/monoexonic_filter/feelnc_as_monoexonic_lncrnas.list

# ensembl annotated lncrnas

# keep lnrnas 1) monoexonic found by blast against rfam 2) synteny,
# 3) antisense to protein coding genes 4) annotated in ensembl and with more than 1 exon
cat \
reference_genome/ensembl_lncrna_transcript.list \
$OUTDIR/monoexonic_filter/not_monoexonic_transcripts.list \
$OUTDIR/monoexonic_filter/rfam_monoexonic_lncrnas.list \
$OUTDIR/monoexonic_filter/human_monoexonic_lncrnas.list \
$OUTDIR/monoexonic_filter/mouse_monoexonic_lncrnas.list \
$OUTDIR/monoexonic_filter/feelnc_as_monoexonic_lncrnas.list \
  $ens_lncrna |  sort| uniq  > lncrna_annotation/monoexonic_filter/monoexonic_retain.list

grep -wFf $OUTDIR/monoexonic_filter/monoexonic_retain.list $stringtie_gtf > $OUTDIR/lncrna.gtf


## rerun classifer on final lncrna list
FEELnc_classifier.pl \
-i $OUTDIR/lncrna.gtf \
-a $ref_gtf \
> $OUTDIR/classification/second_classification.txt
