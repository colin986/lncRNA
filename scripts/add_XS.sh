#!/usr/bin/env bash
while getopts a:g:p:d: option
  do
    case "${option}"
      in
      a) GTF=${OPTARG};;
      g) FASTA=${OPTARG};;
      p) THREADS=${OPTARG};;
      d) GENOMEDIR=${OPTARG};;
    esac
done

samtools view -h ../alt_splicing_analysis/mapping/REP31_1Aligned.sortedByCoord.out.bam | gawk -v strType=2 -f tagXSstrandedData.awk | samtools view -bS - > mapped/REP31_1Aligned.sortedByCoord.out.bam&
samtools view -h ../alt_splicing_analysis/mapping/REP31_2Aligned.sortedByCoord.out.bam | gawk -v strType=2 -f tagXSstrandedData.awk | samtools view -bS - > mapped/REP31_2Aligned.sortedByCoord.out.bam&
samtools view -h ../alt_splicing_analysis/mapping/REP31_3Aligned.sortedByCoord.out.bam | gawk -v strType=2 -f tagXSstrandedData.awk | samtools view -bS - > mapped/REP31_3Aligned.sortedByCoord.out.bam&
samtools view -h ../alt_splicing_analysis/mapping/REP31_4Aligned.sortedByCoord.out.bam | gawk -v strType=2 -f tagXSstrandedData.awk | samtools view -bS - > mapped/REP31_4Aligned.sortedByCoord.out.bam&

samtools view -h ../alt_splicing_analysis/mapping/REP37_1Aligned.sortedByCoord.out.bam | gawk -v strType=2 -f tagXSstrandedData.awk | samtools view -bS - > mapped/REP37_1Aligned.sortedByCoord.out.bam&
samtools view -h ../alt_splicing_analysis/mapping/REP37_2Aligned.sortedByCoord.out.bam | gawk -v strType=2 -f tagXSstrandedData.awk | samtools view -bS - > mapped/REP37_2Aligned.sortedByCoord.out.bam&
samtools view -h ../alt_splicing_analysis/mapping/REP37_3Aligned.sortedByCoord.out.bam | gawk -v strType=2 -f tagXSstrandedData.awk | samtools view -bS - > mapped/REP37_3Aligned.sortedByCoord.out.bam&
samtools view -h ../alt_splicing_analysis/mapping/REP37_4Aligned.sortedByCoord.out.bam | gawk -v strType=2 -f tagXSstrandedData.awk | samtools view -bS - > mapped/REP37_4Aligned.sortedByCoord.out.bam

samtools index mapped/REP31_1Aligned.sortedByCoord.out.bam
samtools index mapped/REP31_2Aligned.sortedByCoord.out.bam
samtools index mapped/REP31_3Aligned.sortedByCoord.out.bam
samtools index mapped/REP31_4Aligned.sortedByCoord.out.bam
samtools index mapped/REP37_1Aligned.sortedByCoord.out.bam
samtools index mapped/REP37_2Aligned.sortedByCoord.out.bam
samtools index mapped/REP37_3Aligned.sortedByCoord.out.bam
samtools index mapped/REP37_4Aligned.sortedByCoord.out.bam
