#!/usr/bin/env bash
if (($# == 0)); then
        echo "Usage:"
        echo "-s = SWISSPROT BLAST output dir"
        echo "-r = RFAM BLAST output dir"
        echo "-m = MIRBASE BLAST output dir"
        echo "-e = E-Value threshold"
        echo "-n = FEELNc lncRNA nucleotide sequence"
        echo "-p = protein sequence"
        echo "-t = Processor numbers"
        exit 2
fi
while getopts r:s:e:p:n:s:m:t: option
  do
    case "${option}"
      in
      r) RFAMOUTPUTDIR=${OPTARG};;
      e) EVALUE=${OPTARG};;
      p) PROTEIN=${OPTARG};;
      n) NUCLEOTIDE=${OPTARG};;
      s) SWISSPROTOUTDIR=${OPTARG};;
      m) MIRBASEOUTDIR=${OPTARG};;
      t) THREADS=${OPTARG};;
    esac
done

# Swiss prot
if [ ! -d $SWISSPROTOUTDIR ]; then
mkdir -p $SWISSPROTOUTDIR
fi

if [ ! -f $SWISSPROTOUTDIR/uniprot_sprot.fasta ]; then
wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz \
-P $SWISSPROTOUTDIR
gunzip $SWISSPROTOUTDIR/uniprot_sprot.fasta.gz
fi

if [ ! -f $SWISSPROTOUTDIR/uniprot_sprot.fasta.phr ]; then
makeblastdb -in $SWISSPROTOUTDIR/uniprot_sprot.fasta -dbtype prot
fi

blastp \
-query $PROTEIN \
-db $SWISSPROTOUTDIR/uniprot_sprot.fasta  \
-max_target_seqs 1 \
-outfmt 6 \
-evalue $EVALUE \
-num_threads $THREADS \
> $SWISSPROTOUTDIR/blastp.outfmt6

#miRBase
if [ ! -d $MIRBASEOUTDIR ]; then
mkdir -p $MIRBASEOUTDIR
fi

if [ ! -f $MIRBASEOUTDIR/hairpin.fa ]; then
wget ftp://mirbase.org/pub/mirbase/CURRENT/hairpin.fa.gz \
-P $MIRBASEOUTDIR
gunzip $MIRBASEOUTDIR/hairpin.fa.gz
fi

if [ ! -f $MIRBASEOUTDIR/hairpin.fa.nhr ]; then
makeblastdb -in $MIRBASEOUTDIR/hairpin.fa -dbtype nucl
fi

blastn \
-db $MIRBASEOUTDIR/hairpin.fa \
-query $NUCLEOTIDE \
-strand plus \
-evalue $EVALUE \
-num_threads $THREADS \
-outfmt 6 \
-num_alignments 1 \
> $MIRBASEOUTDIR/blastn.outfmt6

# RFAM
if [ ! -d $RFAMOUTPUTDIR ]; then
mkdir -p $RFAMOUTPUTDIR
fi

# create 2 files for blast snorna fasta for filtering here and lncRNA for comparison later
if [ ! -f $RFAMOUTPUTDIR/rfam.filter.fasta ] || [ ! -f $RFAMOUTPUTDIR/rfam_lncrna.fasta ] ; then
  wget -r ftp://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/fasta_files/ -P $RFAMOUTPUTDIR
  awk {'print "lncrna_annotation/RFAM/ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/fasta_files/"$1".fa.gz"'} data/extra/rfam_lncrna.info > $RFAMOUTPUTDIR/rfam_lncrna.fasta.list
  awk {'print "lncrna_annotation/RFAM/ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/fasta_files/"$1".fa.gz"'} data/extra/rfam_snorna.info > $RFAMOUTPUTDIR/rfam_snorna.fasta.list
  awk {'print "lncrna_annotation/RFAM/ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/fasta_files/"$1".fa.gz"'} data/extra/rfam_ribozyme.info > $RFAMOUTPUTDIR/rfam_ribozyme.fasta.list

  #lncrna_fasta
  cat $RFAMOUTPUTDIR/rfam_lncrna.fasta.list | while read rfam_fasta; do
      zcat $rfam_fasta >> $RFAMOUTPUTDIR/rfam_lncrna.fasta
    done

  cat $RFAMOUTPUTDIR/rfam_snorna.fasta.list | while read rfam_fasta; do
     zcat $rfam_fasta >> $RFAMOUTPUTDIR/rfam_snorna.fasta
   done

   cat $RFAMOUTPUTDIR/rfam_ribozyme.fasta.list | while read rfam_fasta; do
      zcat $rfam_fasta >> $RFAMOUTPUTDIR/rfam_ribozyme.fasta
    done

    rm -r $RFAMOUTPUTDIR/ftp.ebi.ac.uk
fi

# merge the RFAM sequences to filter against
cat $RFAMOUTPUTDIR/rfam_snorna.fasta $RFAMOUTPUTDIR/rfam_ribozyme.fasta > $RFAMOUTPUTDIR/rfam.filter.fasta
#create blastdb for RFAM lncRNA and non-coding RNA
if [ ! -f $RFAMOUTPUTDIR/rfam.filter.fasta.hnr ]; then
makeblastdb -in $RFAMOUTPUTDIR/rfam.filter.fasta -dbtype nucl
fi

if [ ! -f $RFAMOUTPUTDIR/rfam_lncrna.fasta.hnr ]; then
makeblastdb -in $RFAMOUTPUTDIR/rfam_lncrna.fasta -dbtype nucl
fi

blastn \
-db $RFAMOUTPUTDIR/rfam.filter.fasta \
-query $NUCLEOTIDE \
-strand plus \
-evalue $EVALUE \
-perc_identity 95 \
-num_threads $THREADS \
-outfmt 6 \
-num_alignments 1 \
> $RFAMOUTPUTDIR/blastn.outfmt6


blastn \
-db $RFAMOUTPUTDIR/rfam_lncrna.fasta \
-query $NUCLEOTIDE \
-strand plus \
-evalue $EVALUE \
-perc_identity 90 \
-num_threads $THREADS \
-outfmt 6 \
-num_alignments 1 \
> $RFAMOUTPUTDIR/lncrna.blastn.outfmt6

echo "----------------------"
echo "BLAST steps complete"
echo "----------------------"
echo "protein BLAST output written to $SWISSPROTOUTDIR/blastp.outfmt6"
echo "mriRNA BLAST output written to $MIRBASEOUTDIR/blastn.outfmt6"
echo "snoRNA and ribozyme BLAST output written to $RFAMOUTPUTDIR/blastn.outfmt6"
echo "lncRNA BLAST output written to $RFAMOUTPUTDIR/lncrna.blastn.outfmt6"
