

egrep 'ENSCGRG00000006752|MSTRG.9008' \
stringtie/stringtie_original.gtf > coverage_plots/oxct1/oxct1.gtf

../../bin/kentUtils/bin/linux.x86_64/gtfToGenePred \
coverage_plots/oxct1/oxct1.gtf \
coverage_plots/oxct1/oxct1.genePhred

../../bin/kentUtils/bin/linux.x86_64/genePredToBed \
coverage_plots/oxct1/oxct1.genePhred \
coverage_plots/oxct1/oxct1.bed


/home/colin/miniconda3/bin/pyGenomeTracks \
--tracks coverage_plots/oxct1/genome_track.ini \
--region JH000378.1:453578-576999 \
--outFileName coverage_plots/oxct1/oxct1.png \
--dpi 300 \
--height 15 \
--width 25

###########################################################################

egrep 'ENSCGRG00000017027|MSTRG.11950' \
stringtie/stringtie_original.gtf > coverage_plots/timeless/timeless.gtf

../../bin/kentUtils/bin/linux.x86_64/gtfToGenePred \
coverage_plots/timeless/timeless.gtf \
coverage_plots/timeless/timeless.genePhred

../../bin/kentUtils/bin/linux.x86_64/genePredToBed \
coverage_plots/timeless/timeless.genePhred \
coverage_plots/timeless/timeless.bed


/home/colin/miniconda3/bin/pyGenomeTracks \
--tracks coverage_plots/timeless/genome_track.ini \
--region JH000638.1:312470-339676 \
--outFileName coverage_plots/timeless/timeless.png \
--dpi 300 \
--height 15 \
--width 25

###########################################################################

egrep 'ENSCGRG00000010663|MSTRG.3983' \
stringtie/stringtie_original.gtf > coverage_plots/cbx3/cbx3.gtf

../../bin/kentUtils/bin/linux.x86_64/gtfToGenePred \
coverage_plots/cbx3/cbx3.gtf \
coverage_plots/cbx3/cbx3.genePhred

../../bin/kentUtils/bin/linux.x86_64/genePredToBed \
coverage_plots/cbx3/cbx3.genePhred \
coverage_plots/cbx3/cbx3.bed

/home/colin/miniconda3/bin/pyGenomeTracks \
--tracks coverage_plots/cbx3/genome_track.ini \
--region JH000116.1:650713-665132 \
--outFileName coverage_plots/cbx3/cbx3.png \
--dpi 300 \
--height 15 \
--width 25
