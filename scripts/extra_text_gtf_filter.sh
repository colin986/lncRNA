grep 'protein_coding' reference_genome/ensembl_chok1_genome.gtf | awk {'print $10'} | uniq | tr -d \" | tr -d \; > protein.coding.genes
grep 'lincRNA' reference_genome/ensembl_chok1_genome.gtf | awk {'print $10'} | uniq | tr -d \" | tr -d \;

grep -wFf protein.coding.genes stringtie_output/stringtie.gtf




cat lncrna_annotation/RFAM/rfam_lncRNAs.fasta.list | while read rfam_fasta; do
zcat $rfam_fasta >> delete.lncRNA_fasta
done




grep -wFf $TRANSCRIPTDIR/protein.coding.genes.list $TRANSCRIPTDIR/stringtie.gtf > $TRANSCRIPTDIR/stringtie_protein_coding.gtf


grep -wFf $TRANSCRIPTDIR/protein.coding.genes.list $TRANSCRIPTDIR/stringtie.gtf > $TRANSCRIPTDIR/stringtie_protein_coding.gtf




# GTF for later comparison to lncRNAs
#create a GTF containing only transcripts from annotated protein coding genes in ENSEMBL
grep 'protein_coding' $GTF | awk {'print $10'} | uniq | tr -d \" | tr -d \; > $TRANSCRIPTDIR/protein.coding.genes.list


egrep 'miRNA|Mt_rRNA|Mt_tRNA|processed_pseudogene|pseudogene|ribozyme|rRNA|scaRNA|snoRNA|snRNA|sRNA' $GTF \
| awk {'print $10'} | uniq | tr -d \" | tr -d \; > $TRANSCRIPTDIR/other.noncoding.genes.list
grep -wFf $TRANSCRIPTDIR/other.noncoding.genes.list $TRANSCRIPTDIR/stringtie.gtf > $TRANSCRIPTDIR/stringtie_other_noncoding.gtf

# make lists for lncrNAs annotated in ensembl
grep 'lincRNA' $GTF | awk {'print $10'} | uniq | tr -d \" | tr -d \; > $TRANSCRIPTDIR/ensembl_lncrna_gene.list
grep 'lincRNA'$GTF | awk {'print $14'} | grep -v 'ensembl' | uniq | tr -d \" | tr -d \; > $TRANSCRIPTDIR/ensembl_lncrna_transcript.list

grep -wFf lncrna_annotation/novel_lncrna_transcripts.list lncrna_annotation/FEELnc/feelnc_codpot_out/candidate_lncRNA.gtf.lncRNA.gtf | awk '{print $12}' | uniq | wc -l
grep -wFf reference_genome/ensembl_lncrna_transcript.list lncrna_annotation/FEELnc/feelnc_codpot_out/candidate_lncRNA.gtf.lncRNA.gtf | awk '{print $12}' | uniq | wc -l
grep -wFf lncrna_annotation/all_lncrna_transcripts.list lncrna_annotation/FEELnc/feelnc_codpot_out/candidate_lncRNA.gtf.lncRNA.gtf | awk '{print $12}' | uniq | wc -l



~/bin/TransDecoder-TransDecoder-v5.5.0/util/gtf_genome_to_cdna_fasta.pl lncrna_annotation/all_lncrna.gtf reference_genome/ensembl_chok1_genome.fa > $OUTDIR/candidate_lncRNA.nocodpot.cdna.fa
