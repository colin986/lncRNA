# identificaiton and differential expression analysis of CHO cell long non-coding lncRNA

These scripts replicate the results of the following manuscript

## Installation
### Dependancies

STAR v2.7.2d
gawk
Stringtie-v2.0.3
BLAST
gmap
liftOver

## RNASeq read QC, alignment and genome guided transcriptome assembly
TBC

### get the ensembl CHOK1 genome and GTF
```bash
./scripts/prepare_genome.sh -v 98 -o reference_genome
```

### make a STAR index for alignment
-a = genome fasta file
-g = anntation file
-p = processors

```bash
./scripts/make_star_index.sh -g reference_genome/ensembl_chok1_genome.fa -a reference_genome/ensembl_chok1_genome.gtf -p 32 -d reference_genome
```

### create a list of sample sample_names
```bash
ls ../data/raw/ | sed -n 's/\.fastq.gz$//p' | cut -d_ -f1-2 | uniq > data/sample_names.txt
```

### trim adapter sequences
```bash
cat data/sample_names.txt | while read sample; do
./scripts/trim_adapter.sh -s $sample  -i ../data/raw -o ../data/preprocessed/cutadapt&
done
```

### quality trimming
```bash
cat data/sample_names.txt | while read sample; do
./scripts/trim_quality.sh -s $sample -i ../data/preprocessed/cutadapt -o../data/preprocessed
done
```

### map to CHOK1 genome
```bash
cat data/sample_names.txt | while read sample; do
./scripts/star_mapping.sh -s $sample -i ../data/preprocessed/paired -g reference_genome/star_index -o mapped -p 32
done
```

### string tie assembly [change location of mapped data]
```bash
cat data/sample_names.txt | while read sample; do
./scripts/run_Stringtie.sh -s $sample -i mapped -g reference_genome/ensembl_chok1_genome.gtf -o stringtie -p 32
done
```

### merge individual stringtie assemblies and compare to ENSEMBL annotation
```bash
  ./scripts/stringtie_merge.sh -t stringtie -g reference_genome/ensembl_chok1_genome.gtf
```

## Long non-coding RNA identification
The stringtie transcriptome assembly is used to predict lncRNAs using FEELNc
#### make a directory for the lncRNA annotation steps
```bash
mkdir -p lncrna_annotation
```

#### calculate TPM expression values
```bash
cat data/sample_names.txt | while read sample; do
./scripts/run_TPM.sh -p 32 -s $sample -g stringtie/stringtie.gtf -o lncrna_annotation/TPM -b mapped
done
```

####  aggregate expression for each sample
```bash
./scripts/make_TPM_matrix.sh -s ../data/sample_names.txt -o lncrna_annotation/TPM/
```

#### FEELNc analysis
#The stringtie transcriptome assembly is used to predict lncRNAs using FEELNc
```bash
./scripts/run_FEELnc.sh -G reference_genome/ensembl_chok1_genome.gtf -g stringtie/stringtie.gtf -f reference_genome/ensembl_chok1_genome.fa -o lncrna_annotation/FEELnc
```

#### Use transdecoder to create a cDNA fasta file identify the longest ORF for each candidate lncRNA
```bash
./scripts/run_Transdecoder.sh -g lncrna_annotation/FEELnc/feelnc_codpot_out/candidate_lncRNA.gtf.lncRNA.gtf -f reference_genome/ensembl_chok1_genome.fa -o lncrna_annotation/TRANSDECODER
```

#### CPAT coding prediction for FEELNc candidate lncRNAs
```bash
./scripts/run_CPAT.sh -f lncrna_annotation/TRANSDECODER/candidate_lncRNA.nocodpot.cdna.fa -o lncrna_annotation/CPAT
```

#### CPC2 coding prediction for FEELnc candidate lncRNAs
```bash
  ./scripts/run_CPC2.sh -f lncrna_annotation/TRANSDECODER/candidate_lncRNA.nocodpot.cdna.fa -o lncrna_annotation/CPC2
```

### Assess FEELnc candiate lncRNAs for the presence of protein domains
```bash
./scripts/run_HMMscan.sh -t 32 -e 1e-5 -p lncrna_annotation/TRANSDECODER/longest_orfs.pep -o lncrna_annotation/PFAM
```

### Assess FEELnc candiate lncRNAs for the presence of proteins, miRNAs, and other non-coding RNAs (e.g. snoRNAs) using BLAST
```bash
./scripts/run_BLAST.sh  -t 32 -e 1e-5 -s lncrna_annotation/SWISSPROT -p lncrna_annotation/TRANSDECODER/longest_orfs.pep -m lncrna_annotation/MIRBASE -r lncrna_annotation/RFAM -n lncrna_annotation/TRANSDECODER/candidate_lncRNA.nocodpot.cdna.fa
```
#### Filter FEELNc output using additional protein potential calculators, PFAM search and BLAST against protein and RNA databases
```bash
Rscript R/filter_lncrna.R \
  "lncrna_annotation/FEELnc/feelnc_codpot_out/candidate_lncRNA.gtf.lncRNA.gtf" \
  "lncrna_annotation/CPC2/CPC2.analysis.txt" \
  "lncrna_annotation/CPAT/CPAT.analysis.txt" \
  "lncrna_annotation/SWISSPROT/blastp.outfmt6" \
  "lncrna_annotation/MIRBASE/blastn.outfmt6" \
  "lncrna_annotation/PFAM/pfam_domain_transcripts" \
  "lncrna_annotation/RFAM/blastn.outfmt6" \
  "lncrna_annotation/TPM/transcript_tpm_all_samples.tsv" \
  "lncrna_annotation/FEELnc/lncRNA_classes.txt" \
  "stringtie/stringtie.gtf" \
  "lncrna_annotation"
```
## Annotate and Classify CHO cell RNAs lncRNAs
#### determine synteny with human and mouse gencode lncRNAs
./scripts/annotate_lncRNA.sh -s lncrna_annotation/firstpass_filter/lncRNA.fasta -o lncrna_annotation

## Filter monoexonic lncRNAs
#### keep only monoexonic lncRNAs that are:
#### 1) antisense to a protein coding gene 2) are orthologous with human or mouse 3) annotated as lncRNA in ensembl
```bash
./scripts/filter_monoexonic.sh \
-o lncrna_annotation \
-g reference_genome/ensembl_chok1_protein.gtf \
-l lncrna_annotation/firstpass_filter/all_lncrna.gtf \
-k reference_genome/ensembl_lncrna_transcript.list \
-a stringtie/stringtie.gtf
```

#### determine the proximity and orientation of lncRNAs wrt to protein coding genes
```bash
./scripts/classify_lncRNA.sh -G reference_genome/ensembl_chok1_genome.gtf -o lncrna_annotation
```

## Simplify FEELnc classifications
#### determine the proximity and orientation of lncRNAs wrt to protein coding genes
```bash
Rscript R/simplify_class.R \
  "lncrna_annotation/classification/second_classification.txt" \
  "lncrna_annotation/classification/final_classification.txt"
```


## Differential expression analysis
#### Gene level counting
```bash
mkdir differential_expression
cat ../data/sample_names.txt | while read sample; do
./scripts/htseq_count.sh -s $sample -m mapped -g stringtie/stringtie.gtf -o differential_expression/counts&
done
```

### DESeq2 analysis
```bash
Rscript R/run_DEseq2.R \
  "differential_expression/counts" \
  "differential_expression/DESeq2_results"
```
## Manuscript counts
```bash
Rscript R/manuscript_calculations.R
```
